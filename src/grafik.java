import java.awt.Color;
import java.awt.Window.Type;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JComboBox;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;

;
public class grafik extends JFrame {

	private JComboBox comboBox;
	private Connection con;
	private String user, h;
	private JPanel panel;
	private ChartFrame chart;
	private Date date;
	private JLabel waktu;
	private ChartPanel fr;

	public grafik(String a) {
		setSize(600, 600);
		setType(Type.POPUP);
		setLocationRelativeTo(null);
		getContentPane().setLayout(null);

		panel = new JPanel();
		panel.setBounds(0, 0, 600, 600);
		panel.setBackground(Color.WHITE);
		getContentPane().add(panel);
		panel.setLayout(null);

		comboBox = new JComboBox();
		comboBox.setBounds(220, 13, 176, 34);
		comboBox.setAlignmentX(CENTER_ALIGNMENT);
		comboBox.setAlignmentY(CENTER_ALIGNMENT);
		comboBox.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				tmpil(user);
				refres();
			}
		});
		panel.add(comboBox);

		user = a;
		delcek(user);
		cek(user);
	}

	private static Connection getConnection() throws Exception {
		Class.forName("com.mysql.jdbc.Driver");
		String url = "jdbc:mysql://localhost/monefy";
		String user = "root";
		String pass = "";
		return DriverManager.getConnection(url, user, pass);
	}

	public void tangall() {
		Date now = new Date();
		String pattern = "yyyy-MM-dd";
		SimpleDateFormat formatter = new SimpleDateFormat(pattern);
		String mysqlDateString = formatter.format(now);
		waktu = new JLabel(mysqlDateString);
	}

	public void cek(String user) {
		try {
			con = getConnection();
			PreparedStatement st = con
					.prepareStatement("Select distinct pengeluaran.tanggal from pengeluaran where username='"
							+ user + "'");
			ResultSet ss = st.executeQuery();
			while (ss.next()) {
				Date dt1 = ss.getDate("tanggal");
				DateFormat dt = new SimpleDateFormat("dd-MM-yyyy");
				h = dt.format(dt1);
				comboBox.addItem(h);
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Cek Koneksi Anda");
		}
	}

	public void delcek(String user) {
		while (comboBox.getItemCount() > 0) {
			for (int i = 0; i < comboBox.getItemCount(); i++)
				comboBox.removeItemAt(i);
		}
	}

	public void tmpil(String user) {

		String pattern = "dd-MM-yyyy";
		String pattern2 = "yyyy-MM-dd";
		SimpleDateFormat formatter = new SimpleDateFormat(pattern);
		SimpleDateFormat formatter2 = new SimpleDateFormat(pattern2);

		try {

			String a = comboBox.getSelectedItem().toString();

			// System.out.println(a);

			String date = formatter2.format(formatter.parse(a));
			con = getConnection();
			PreparedStatement sd = con
					.prepareStatement("SELECT DISTINCT DATE('"
							+ date
							+ "'), kategori, SUM( value ) AS total FROM pengeluaran WHERE username = '"
							+ user + "' AND tanggal = DATE( '" + date
							+ "') GROUP BY kategori");
			ResultSet de = sd.executeQuery();
			DefaultPieDataset dataset = new DefaultPieDataset();
			DefaultCategoryDataset dataset1 = new DefaultCategoryDataset();
			while (de.next()) {
				dataset1.setValue(de.getInt("total"), date,
						de.getString("kategori"));
				dataset.setValue(de.getString("kategori"),
						Double.parseDouble(de.getString("total")));
			}
			JFreeChart chart2 = ChartFactory.createBarChart(
					"Garfik pengeluaran", "Kategori", "Total", dataset1,
					PlotOrientation.VERTICAL, false, true, false);

			JFreeChart chart = ChartFactory.createPieChart(
					"Grafik Pengeluaran", dataset, true, true, false);
			fr = new ChartPanel(chart);
			// fr = new ChartPanel(chart);
			fr.setSize(570, 440);
			fr.setLocation(5, 60);
			fr.setVisible(true);
			panel.add(fr);
		} catch (Exception e1) {
			JOptionPane.showMessageDialog(null, "Cek Koneksi Anda");
			e1.printStackTrace();
		}
	}

	public void refres() {
		panel.removeAll();
		panel.revalidate();
		// ChartPanel chartPanel = new ChartPanle(chart1);
		panel.add(comboBox);
		panel.add(fr);
		panel.repaint();
	}

}
