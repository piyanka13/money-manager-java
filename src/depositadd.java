import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import java.awt.Color;
import java.awt.Font;
import java.awt.Window.Type;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;

public class depositadd extends JFrame implements ActionListener {

	private JLabel lblNewLabel, label, lbjumlah, lbsimpan, lbjlh, lbatm,
			lbuser, lbwaktu, lbcash, lbket, lbtotal, lbtmbh, lbtotal1,
			lbtotcash;
	private JButton btnSubmit, btnatm, btnsave, btncancel, btnsave1,
			btncancel1;
	private JPanel panel_1, paneladd, panelatm;
	private JTextField txtadd, txtatm;
	private String user, waktu;
	private JLabel lblNewLabel_1;
	private Connection con;

	public depositadd(String a, String b) {
		setType(Type.POPUP);
		setSize(378, 378);
		setLocationRelativeTo(null);
		getContentPane().setLayout(null);

		panel_1 = new JPanel();
		panel_1.setBounds(0, 0, 361, 332);
		panel_1.setBackground(new Color(152, 251, 152));
		getContentPane().add(panel_1);
		panel_1.setLayout(null);

		lblNewLabel = new JLabel("Your Deposit");
		lblNewLabel.setFont(new Font("Segoe Print", Font.BOLD, 23));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(86, 13, 202, 48);
		panel_1.add(lblNewLabel);

		label = new JLabel("");
		label.setFont(new Font("Tahoma", Font.BOLD, 20));
		label.setBounds(201, 123, 123, 34);
		panel_1.add(label);

		btnSubmit = new JButton("Set Simpanan");
		btnSubmit.setFont(new Font("Segoe Print", Font.BOLD, 15));
		btnSubmit.setBounds(12, 269, 150, 50);
		btnSubmit.addActionListener(this);
		panel_1.add(btnSubmit);

		btnatm = new JButton("Penarikkan");
		btnatm.setFont(new Font("Segoe Print", Font.BOLD, 16));
		btnatm.setBounds(195, 269, 154, 50);
		btnatm.addActionListener(this);
		panel_1.add(btnatm);

		lblNewLabel_1 = new JLabel("Rp");
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel_1.setFont(new Font("Serif", Font.BOLD, 27));
		lblNewLabel_1.setBounds(12, 123, 108, 34);
		panel_1.add(lblNewLabel_1);

		paneladd = new JPanel();
		paneladd.setBounds(0, 0, 361, 332);
		paneladd.setBackground(new Color(152, 251, 152));
		paneladd.setLayout(null);

		lbsimpan = new JLabel("Simpanan");
		lbsimpan.setFont(new Font("Segoe Print", Font.BOLD, 23));
		lbsimpan.setHorizontalAlignment(SwingConstants.CENTER);
		lbsimpan.setBounds(86, 13, 202, 48);
		paneladd.add(lbsimpan);

		lbjumlah = new JLabel("Jumlah Rp");
		lbjumlah.setHorizontalAlignment(SwingConstants.RIGHT);
		lbjumlah.setFont(new Font("Serif", Font.BOLD, 20));
		lbjumlah.setBounds(12, 123, 108, 34);
		paneladd.add(lbjumlah);

		txtadd = new JTextField();
		txtadd.setFont(new Font("Tahoma", Font.BOLD, 14));
		txtadd.setBounds(170, 123, 143, 34);
		paneladd.add(txtadd);

		btnsave = new JButton("Save");
		btnsave.setFont(new Font("Segoe Print", Font.BOLD, 15));
		btnsave.setBounds(12, 269, 141, 50);
		btnsave.addActionListener(this);
		paneladd.add(btnsave);

		btncancel = new JButton("Cancel");
		btncancel.setFont(new Font("Segoe Print", Font.BOLD, 16));
		btncancel.setBounds(195, 269, 154, 50);
		btncancel.addActionListener(this);
		paneladd.add(btncancel);

		panelatm = new JPanel();
		panelatm.setBounds(0, 0, 361, 332);
		panelatm.setBackground(new Color(152, 251, 152));
		panelatm.setLayout(null);

		lbatm = new JLabel("Penarikkan");
		lbatm.setFont(new Font("Segoe Print", Font.BOLD, 23));
		lbatm.setHorizontalAlignment(SwingConstants.CENTER);
		lbatm.setBounds(86, 13, 202, 48);
		panelatm.add(lbatm);

		lbjlh = new JLabel("Jumlah Rp");
		lbjlh.setHorizontalAlignment(SwingConstants.RIGHT);
		lbjlh.setFont(new Font("Serif", Font.BOLD, 20));
		lbjlh.setBounds(12, 123, 108, 34);
		panelatm.add(lbjlh);

		txtatm = new JTextField();
		txtatm.setFont(new Font("Tahoma", Font.BOLD, 14));
		txtatm.setBounds(170, 123, 143, 34);
		panelatm.add(txtatm);

		btnsave1 = new JButton("Save");
		btnsave1.setFont(new Font("Segoe Print", Font.BOLD, 15));
		btnsave1.setBounds(12, 269, 141, 50);
		btnsave1.addActionListener(this);
		panelatm.add(btnsave1);

		btncancel1 = new JButton("Cancel");
		btncancel1.setFont(new Font("Segoe Print", Font.BOLD, 16));
		btncancel1.setBounds(195, 269, 154, 50);
		btncancel1.addActionListener(this);
		panelatm.add(btncancel1);

		user = a;
		lbuser = new JLabel(a);
		lbcash = new JLabel();
		lbket = new JLabel("ATM");
		ShowRprt();
	}

	private static Connection getConnection() throws Exception {
		Class.forName("com.mysql.jdbc.Driver");
		String url = "jdbc:mysql://localhost/monefy";
		String user = "root";
		String pass = "";
		return DriverManager.getConnection(url, user, pass);
	}

	public void tangall() {
		Date now = new Date();
		String pattern = "yyyy-MM-dd";
		SimpleDateFormat formatter = new SimpleDateFormat(pattern);
		String mysqlDateString = formatter.format(now);
		lbwaktu = new JLabel(mysqlDateString);
	}

	public void ShowRprt() {
		try {
			con = getConnection();
			String sql = "SELECT deposito.deposito, cash.ugmasuk FROM deposito, cash WHERE cash.username =  '"
					+ user + "' AND deposito.username = '" + user + "'";
			Statement set = con.createStatement();
			ResultSet rs = set.executeQuery(sql);
			while (rs.next()) {
				label.setText(rs.getString("deposito.deposito"));
				lbcash.setText(rs.getString("cash.ugmasuk"));
			}
			set.close();
			con.close();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "terjadi kesalahan");
		}
	}

	public void lapor1() {
		try {
			tangall();
			con = getConnection();
			PreparedStatement stm = con
					.prepareStatement("UPDATE deposito set deposito=? where username='"
							+ user + "'");
			PreparedStatement stm1 = con
					.prepareStatement("INSERT INTO reportaddbank values(default,?,?,?,?,?)");

			stm.setString(1, lbtotal.getText());

			stm1.setString(1, lbuser.getText());
			stm1.setString(2, lbwaktu.getText());
			stm1.setString(3, label.getText());
			stm1.setString(4, txtadd.getText());
			stm1.setString(5, lbtotal.getText());

			stm.executeUpdate();
			stm1.executeUpdate();
			JOptionPane.showMessageDialog(null, "Terima Kasih");
			stm.close();
			stm1.close();
			con.close();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Cek Koneksi Anda");
		}
	}

	public void add() {
		int depo, depo1, tot;
		String awal = label.getText();
		depo = Integer.parseInt(awal);
		String add = txtadd.getText();
		depo1 = Integer.parseInt(add);
		tot = depo + depo1;
		lbtotal = new JLabel(Integer.toString(tot));
		lapor1();
	}

	public void lapor() {
		try {
			tangall();
			con = getConnection();
			PreparedStatement st = con
					.prepareStatement("INSERT INTO penarikanbank values(default,?,?,?,?,?)");
			PreparedStatement st1 = con
					.prepareStatement("UPDATE deposito set deposito=? where username='"
							+ user + "'");
			PreparedStatement st2 = con
					.prepareStatement("UPDATE cash set ugmasuk=? where username='"
							+ user + "'");
			PreparedStatement st3 = con
					.prepareStatement("INSERT INTO reportaddcash values(default,?,?,?,?,?,?)");

			st.setString(1, lbuser.getText());
			st.setString(2, lbwaktu.getText());
			st.setString(3, label.getText());
			st.setString(4, txtatm.getText());
			st.setString(5, lbtotal1.getText());

			st1.setString(1, lbtotal1.getText());

			st2.setString(1, lbtotcash.getText());

			st3.setString(1, lbuser.getText());
			st3.setString(2, lbwaktu.getText());
			st3.setString(3, lbcash.getText());
			st3.setString(4, lbtmbh.getText());
			st3.setString(5, lbket.getText());
			st3.setString(6, lbtotcash.getText());

			st.executeUpdate();
			st1.executeUpdate();
			st2.executeUpdate();
			st3.executeUpdate();
			JOptionPane.showMessageDialog(null, "Transaksi Berhasil");
			st.close();
			st1.close();
			st2.close();
			st3.close();
			con.close();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Cek Koneksi Anda");
		}
	}

	public void atm() {
		int awal1, tot1, min, cash1, cash2, totcash;
		String depo = label.getText();
		awal1 = Integer.parseInt(depo);
		String minn = txtatm.getText();
		min = Integer.parseInt(minn);
		String cash = lbcash.getText();
		cash1 = Integer.parseInt(cash);

		if (awal1 >= min) {
			tot1 = awal1 - min;
			totcash = cash1 + min;
			lbtotal1 = new JLabel(Integer.toString(tot1));
			lbtotcash = new JLabel(Integer.toString(totcash));
			lbtmbh = new JLabel(Integer.toString(min));
			lapor();
		} else {
			JOptionPane.showMessageDialog(null, "Saldo Anda Tidak Cukup!");
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub

		if (e.getSource() == btnSubmit) {
			remove(panel_1);
			remove(panelatm);
			getContentPane().add(paneladd);
			ShowRprt();
			repaint();
		} else if (e.getSource() == btnatm) {
			remove(panel_1);
			remove(paneladd);
			getContentPane().add(panelatm);
			ShowRprt();
			repaint();
		} else if (e.getSource() == btnsave) {
			if (txtadd.getText().equals("")) {
				JOptionPane.showMessageDialog(null, "Isi Fieldnya");
			} else {
				remove(paneladd);
				remove(panelatm);
				add();
				txtadd.setText("");
				getContentPane().add(panel_1);
				ShowRprt();
				repaint();
			}
		} else if (e.getSource() == btncancel) {
			remove(paneladd);
			remove(panelatm);
			getContentPane().add(panel_1);
			ShowRprt();
			repaint();
		} else if (e.getSource() == btnsave1) {
			if (txtatm.getText().equals("")) {
				JOptionPane.showMessageDialog(null, "Isi Fieldnya");
			} else {
				remove(paneladd);
				remove(panelatm);
				atm();
				txtatm.setText("");
				getContentPane().add(panel_1);
				ShowRprt();
				repaint();
			}
		} else if (e.getSource() == btncancel1) {
			remove(paneladd);
			remove(panelatm);
			getContentPane().add(panel_1);
			ShowRprt();
			repaint();
		}

	}
}
