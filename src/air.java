import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JOptionPane;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JPanel;

import java.awt.GridLayout;

import javax.swing.border.TitledBorder;
import javax.swing.JLabel;

import java.awt.Font;
import java.awt.Window.Type;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.SwingConstants;
import javax.swing.JTextField;

public class air extends JFrame implements ActionListener {
	private String cash2, user, wakt;
	private JLabel username, lblBacaan, waktu, ugmasuk, total,
			lblPengeluaranRp;
	private JPanel panel_1;
	private JTextField textField;
	private JButton btnNewButton;
	private Connection con;

	public air(String a, String b) {
		// setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(378, 369);
		setType(Type.UTILITY);
		setLocationRelativeTo(null);
		getContentPane().setLayout(null);

		panel_1 = new JPanel();
		panel_1.setBackground(new Color(153, 255, 153));
		panel_1.setBounds(0, 0, 360, 322);
		getContentPane().add(panel_1);
		panel_1.setLayout(null);

		lblBacaan = new JLabel("Tarif Air");
		lblBacaan.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblBacaan.setHorizontalAlignment(SwingConstants.CENTER);
		lblBacaan.setBounds(121, 43, 138, 30);
		panel_1.add(lblBacaan);

		lblPengeluaranRp = new JLabel("Pengeluaran Rp.");
		lblPengeluaranRp.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblPengeluaranRp.setBounds(12, 134, 160, 40);
		panel_1.add(lblPengeluaranRp);

		textField = new JTextField();
		textField.setBounds(202, 136, 146, 40);
		panel_1.add(textField);
		textField.setColumns(10);

		btnNewButton = new JButton("Submit");
		btnNewButton.setFont(new Font("Segoe Print", Font.BOLD, 16));
		btnNewButton.setBounds(121, 262, 114, 47);
		btnNewButton.addActionListener(this);
		panel_1.add(btnNewButton);

		user = a;
		username = new JLabel(a);
		ugmasuk = new JLabel();
		awal(user);
	}

	private static Connection getConnection() throws Exception {
		Class.forName("com.mysql.jdbc.Driver");
		String url = "jdbc:mysql://localhost/monefy";
		String user = "root";
		String pass = "";
		return DriverManager.getConnection(url, user, pass);
	}

	public void kat(){
		try{
			con =getConnection();
			
		}catch(Exception e){
			
		}
	}
	
	public void awal(String user) {
		try{
			con = getConnection();
			String sql = "SELECT cash.ugmasuk from cash where username='" +user
					+ "'";
			Statement set = con.createStatement();
			ResultSet rs = set.executeQuery(sql);
			while (rs.next()) {
				ugmasuk.setText(rs.getString("cash.ugmasuk"));
			}
			set.close();
			con.close();
		}catch(Exception e){
			JOptionPane.showMessageDialog(null, "Gagal memproses");
		}
	}
	public void sisa(){
		int value, cash, tot = 0;
		String value1 = textField.getText();
		value = Integer.parseInt(value1);
		String cash1 = ugmasuk.getText();
		cash = Integer.parseInt(cash1);
		if (cash >= value) {
			tot = cash - value;
			total = new JLabel(Integer.toString(tot));
			lapor();
		} else {
			JOptionPane.showMessageDialog(null, "Saldo Anda Tidak mencukupi!");
		}
		
	}

	public void tangall(){
		    Date now = new Date();
	        String pattern = "yyyy-MM-dd";
	        SimpleDateFormat formatter = new SimpleDateFormat(pattern);
	        String mysqlDateString = formatter.format(now);
	        waktu =new JLabel(mysqlDateString);
	}
	public void lapor(){
		try {
			tangall();
			con = getConnection();
			PreparedStatement st = con
					.prepareStatement("INSERT INTO pengeluaran values(default,?,?,?,?,?,?)");
			PreparedStatement st1 = con
					.prepareStatement("Update cash set ugmasuk =? where username='"
							+ user + "'");
			st.setString(1, username.getText());
			st.setString(2, waktu.getText());
			st.setString(3, lblBacaan.getText());
			st.setString(4, textField.getText());
			st.setString(5, ugmasuk.getText());
			st.setString(6, total.getText());

			st1.setString(1, total.getText());
			st1.executeUpdate();
			st.executeUpdate();
			JOptionPane.showMessageDialog(null, "Terima Kasih");
			st.close();
			st1.close();
			con.close();
		} catch (Exception e1) {
			JOptionPane.showMessageDialog(null, "Cek Koneksi Anda");
		}
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if (e.getSource() == btnNewButton) {
			sisa();
			textField.setText("");
	}
}
}