import java.awt.Button;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class login extends JFrame implements ActionListener {

	private JPanel panel, panel2;
	private JLabel label, label2, label3, label4, lb1, lb2, lb3, lb4, lb5, lb6,
			lb7, lb8;
	private JButton b, b1, b2, b3;
	private JTextField txt1, txt3, txt4, txt5, txt6;
	private ImageIcon icon;
	private JPasswordField txt2, txt8;
	private Connection con;
	private JComboBox list, list1;

	public login() {
		super("Money Manager");
		setSize(320, 420);
		setLocationRelativeTo(null);
		setType(Type.POPUP);

		panel = new JPanel();
		panel.setLayout(null);
		panel.setBackground(new Color(152, 251, 152));
		panel.setSize(320, 420);
		getContentPane().add(panel);

		panel2 = new JPanel();
		panel2.setLayout(null);
		panel2.setBackground(new Color(152, 251, 152));
		panel2.setSize(320, 420);

		lb1 = new JLabel("REGISTER");
		lb1.setHorizontalAlignment(SwingConstants.CENTER);
		lb1.setFont(new Font("Arial", Font.BOLD, 16));
		lb1.setSize(110, 44);
		lb1.setLocation(105, 0);
		panel2.add(lb1);

		lb2 = new JLabel("Nama ");
		lb2.setFont(new Font("Arial", Font.BOLD, 13));
		lb2.setSize(90, 25);
		lb2.setLocation(65, 40);
		panel2.add(lb2);

		txt3 = new JTextField();
		txt3.setFont(new Font("Arial", Font.BOLD, 13));
		txt3.setSize(110, 25);
		txt3.setLocation(160, 40);
		panel2.add(txt3);

		lb7 = new JLabel("Jenis Kelmain");
		lb7.setFont(new Font("Arial", Font.BOLD, 13));
		lb7.setSize(90, 25);
		lb7.setLocation(65, 80);
		String a[] = { "Pilih", "Perempuan", "Pria" };
		panel2.add(lb7);

		list = new JComboBox(a);
		list.setSize(110, 25);
		list.setLocation(160, 80);
		list.addActionListener(this);
		panel2.add(list);

		lb3 = new JLabel("Alamat ");
		lb3.setFont(new Font("Arial", Font.BOLD, 13));
		lb3.setSize(90, 25);
		lb3.setLocation(65, 120);
		panel2.add(lb3);

		txt4 = new JTextField();
		txt4.setFont(new Font("Arial", Font.BOLD, 13));
		txt4.setSize(110, 25);
		txt4.setLocation(160, 120);
		panel2.add(txt4);

		lb4 = new JLabel("Usia ");
		lb4.setFont(new Font("Arial", Font.BOLD, 13));
		lb4.setSize(90, 25);
		lb4.setLocation(65, 160);
		panel2.add(lb4);

		txt5 = new JTextField();
		txt5.setFont(new Font("Arial", Font.BOLD, 13));
		txt5.setSize(110, 25);
		txt5.setLocation(160, 160);
		panel2.add(txt5);

		lb5 = new JLabel("Username ");
		lb5.setFont(new Font("Arial", Font.BOLD, 13));
		lb5.setSize(90, 25);
		lb5.setLocation(65, 200);
		panel2.add(lb5);

		txt6 = new JTextField();
		txt6.setFont(new Font("Arial", Font.BOLD, 13));
		txt6.setSize(110, 25);
		txt6.setLocation(160, 200);
		panel2.add(txt6);

		lb6 = new JLabel("Password ");
		lb6.setFont(new Font("Arial", Font.BOLD, 13));
		lb6.setSize(90, 25);
		lb6.setLocation(65, 240);
		panel2.add(lb6);

		txt8 = new JPasswordField();
		txt8.setFont(new Font("Arial", Font.BOLD, 13));
		txt8.setSize(110, 25);
		txt8.setLocation(160, 240);
		panel2.add(txt8);

		icon = new ImageIcon("src/image/wallet.png");

		label = new JLabel(icon);
		label.setSize(100, 100);
		label.setLocation(100, 10);
		panel.add(label);

		label2 = new JLabel("Money Manager");
		label2.setFont(new Font("Arial", Font.BOLD, 13));
		label2.setSize(110, 100);
		label2.setLocation(100, 75);
		panel.add(label2);

		label3 = new JLabel("Username");
		label3.setFont(new Font("Arial", Font.BOLD, 13));
		label3.setSize(130, 25);
		label3.setLocation(60, 152);
		panel.add(label3);

		txt1 = new JTextField();
		txt1.setFont(new Font("Arial", Font.BOLD, 13));
		txt1.setSize(110, 25);
		txt1.setLocation(150, 152);
		panel.add(txt1);

		label4 = new JLabel("Password");
		label4.setFont((new Font("Arial", Font.BOLD, 13)));
		label4.setSize(130, 25);
		label4.setLocation(60, 200);
		panel.add(label4);

		txt2 = new JPasswordField();
		txt2.setFont(new Font("Arial", Font.BOLD, 13));
		txt2.setSize(110, 25);
		txt2.setLocation(150, 200);
		panel.add(txt2);

		b1 = new JButton("Login");
		b1.setFont(new Font("Arial", Font.BOLD, 13));
		b1.setSize(100, 30);
		b1.setLocation(45, 260);
		b1.addActionListener(this);
		panel.add(b1);

		b = new JButton("SignUp");
		b.setFont(new Font("Arial", Font.BOLD, 13));
		b.setSize(100, 30);
		b.setLocation(160, 260);
		b.addActionListener(this);
		panel.add(b);

		b2 = new JButton("Submit");
		b2.setFont(new Font("Arial", Font.BOLD, 13));
		b2.setSize(100, 30);
		b2.setLocation(55, 320);
		b2.addActionListener(this);
		panel2.add(b2);

		b3 = new JButton("Cancel");
		b3.setFont(new Font("Arial", Font.BOLD, 13));
		b3.setSize(100, 30);
		b3.setLocation(175, 320);
		b3.addActionListener(this);
		panel2.add(b3);

	}

	public String getuser() {
		String a;
		a = txt1.getText();
		return a;
	}

	private static Connection getConnection() throws Exception {
		Class.forName("com.mysql.jdbc.Driver");
		String url = "jdbc:mysql://localhost/monefy";
		String user = "root";
		String pass = "";
		return DriverManager.getConnection(url, user, pass);
	}

	public void actionPerformed(ActionEvent arg0) {
		String s = arg0.getActionCommand();
		Connection con;
		if (s.equalsIgnoreCase("Login")) {
			if (txt1.getText().equals("") || txt2.getText().equals("")) {
				JOptionPane.showMessageDialog(null, "Isi Semua Fieldnya!");
			} else {
				try {
					con = getConnection();
					Statement stmt = con.createStatement();
					String sql = "select * from login where username='"
							+ txt1.getText() + "' && password='"
							+ txt2.getText() + "'";
					ResultSet r = stmt.executeQuery(sql);

					if (r.next()) {
						if (txt1.getText().equals(r.getString("username"))
								&& txt2.getText().equals(
										r.getString("password"))) {
							JOptionPane.showMessageDialog(null,
									"Login berhasil");
							setVisible(false);
							beranda b = new beranda(r.getString("username"));
							b.setVisible(true);
						}
					} else {
						JOptionPane.showMessageDialog(null,
								"Anda belum terdaftar");
					}

					stmt.close();
					con.close();
					setDefaultCloseOperation(EXIT_ON_CLOSE);
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, "Terjadi Kesalahan");
				}
			}
		} else if (s.equalsIgnoreCase("SignUp")) {
			panel.setVisible(false);
			panel2.setVisible(true);
			getContentPane().add(panel2);
		} else if (s.equalsIgnoreCase("Submit")) {
			String nama = txt3.getText();
			String jenkel = list.getSelectedItem().toString();
			String alamat = txt4.getText();
			String Usia = txt5.getText();
			String User = txt6.getText();
			String pass = txt8.getText();

			if (nama.equals("") || alamat.equals("") || jenkel.equals("")
					|| Usia.equals("") || User.equals("") || pass.equals("")) {
				JOptionPane.showMessageDialog(null, "Isi Semua Fieldnya!");
			} else {
				try {
					con = getConnection();
					PreparedStatement st = con
							.prepareStatement("INSERT INTO monefy.register values (default,?,?,?,?,?,?)");
					PreparedStatement st1 = con
							.prepareStatement("INSERT INTO login values (default,?,?)");
					PreparedStatement st2 = con
							.prepareStatement("INSERT INTO cash values (default,?,0)");
					PreparedStatement st3 = con
							.prepareStatement("INSERT INTO deposito values(default,?,0)");
					st.setString(1, txt3.getText());
					st.setString(2, list.getSelectedItem().toString());
					st.setString(3, txt4.getText());
					st.setString(4, txt5.getText());
					st.setString(5, txt6.getText());
					st.setString(6, txt8.getText());

					st1.setString(1, txt6.getText());
					st1.setString(2, txt8.getText());

					st2.setString(1, txt6.getText());

					st3.setString(1, txt6.getText());

					st.executeUpdate();
					st1.executeUpdate();
					st2.executeUpdate();
					st3.executeUpdate();
					JOptionPane.showMessageDialog(null, "Register Berhasil ");
					st.close();
					st1.close();
					st2.close();
					st3.close();
					con.close();
					panel2.setVisible(false);
					panel.setVisible(true);
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null,
							"Username Sudah Tersedia");
				}
			}
		} else if (s.equalsIgnoreCase("Cancel")) {
			panel2.setVisible(false);
			panel.setVisible(true);
		}

	}
}