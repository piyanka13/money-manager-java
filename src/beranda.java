import java.awt.*;

import javax.swing.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.print.PrinterException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import java.util.Calendar;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.eclipse.wb.swing.FocusTraversalOnArray;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.DefaultPieDataset;

import java.awt.Component;

import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
import java.awt.print.*;

public class beranda extends JFrame implements ActionListener {

	private JPanel isi, pnlreport, pnlkategori, profil;
	private ImageIcon user1;
	private ChartPanel e;
	private DefaultPieDataset dataset;
	private JFreeChart chart;
	private JComboBox tanggal;
	private JLabel usernama, total, total1, lblNewLabel_1, lbketer, lbpinjam,
			lbbook, lblistrik, lbtransport, lbmakan, lbbelanja, lbpulsa, lbair,
			lbinternet, lbjajan, lbbaju, lbhiburan, lbtabungan, lblNewLabel,
			lblkate, lblWelcomeToMoney, lblreport, lb, lbDeposit, lblCashRp,
			lbkurang, lbmasuk, lbsaving, lbtarik;
	private JTextField txtdeposit, txtcash, txtkurang, txtketerangan;
	private String a, b, add;
	private JButton btnReport, btnSave, btncancel, btnKurangUang, btncancel1,
			btnbook, btnlistrik, btntransport, btnmakan, btnbelanja, btnpulsa,
			btnair, btninternet, btnjajan, btnbaju, btnhiburan, btntabungan,
			btnEntryData, btnrept, btnsaving, btnpdf, btnpdf1, btnpdf2,
			btngrafik, btncash, btnLogout, btnDetail, btnSubmit,
			btnSetUangAnda;;
	private JLabel lblNewLabel_2, lbuser, lblNowYourBalance;
	private JTable tabReport, tabReport1, tabReport2, tabReport3, tabReport4;
	Connection con;
	private JPanel panel_1, paneladd, panelminus;
	private DefaultTableModel dbModel, dbModel1, dbModel2, dbModel3, dbModel4;
	private JScrollPane tblscroll, tblscroll1, tblscroll2, tblscroll3,
			tblscroll4;

	public beranda(String user) {

		Date now = new Date();
		String pattern = "yyyy-MM-dd";
		SimpleDateFormat formatter = new SimpleDateFormat(pattern);
		String mysqlDateString = formatter.format(now);

		a = user;
		lbuser = new JLabel(a);

		setExtendedState(MAXIMIZED_BOTH);
		setLocationRelativeTo(null);
		getContentPane().setLayout(null);

		profil = new JPanel();
		profil.setBounds(0, 0, 193, 693);
		profil.setBorder(null);
		profil.setBackground(new Color(204, 255, 153));
		profil.setLayout(null);
		getContentPane().add(profil);

		user1 = new ImageIcon("src/image/user1.png");
		lblNewLabel_1 = new JLabel(user1);
		lblNewLabel_1.setBounds(40, 50, 126, 118);
		profil.add(lblNewLabel_1);

		usernama = new JLabel("Hello " + user);
		usernama.setHorizontalAlignment(SwingConstants.CENTER);
		usernama.setFont(new Font("Segoe Print", Font.BOLD, 19));
		usernama.setBounds(9, 171, 182, 53);
		profil.add(usernama);

		btnReport = new JButton("Balance");
		btnReport.setBorder(BorderFactory.createEmptyBorder());
		btnReport.setBackground(new Color(204, 255, 153));
		btnReport.setFont(new Font("Segoe Print", Font.BOLD, 17));
		btnReport.setBounds(40, 262, 126, 50);
		btnReport.addActionListener(this);
		profil.add(btnReport);

		btnEntryData = new JButton("Pengeluaran");
		btnEntryData.setBorder(BorderFactory.createEmptyBorder());
		btnEntryData.setBackground(new Color(204, 255, 153));
		btnEntryData.setFont(new Font("Segoe Script", Font.BOLD, 18));
		btnEntryData.setBounds(40, 371, 126, 50);
		btnEntryData.addActionListener(this);
		profil.add(btnEntryData);

		btnDetail = new JButton("Report");
		btnDetail.setBorder(BorderFactory.createEmptyBorder());
		btnDetail.setBackground(new Color(204, 255, 153));
		btnDetail.addActionListener(this);
		btnDetail.setFont(new Font("Segoe Script", Font.BOLD, 18));
		btnDetail.setBounds(40, 483, 126, 50);
		profil.add(btnDetail);

		btnLogout = new JButton("Logout");
		btnLogout.setBorder(BorderFactory.createEmptyBorder());
		btnLogout.setFont(new Font("Segoe Script", Font.BOLD, 18));
		btnLogout.setBackground(new Color(204, 255, 153));
		btnLogout.addActionListener(this);
		btnLogout.setBounds(40, 603, 126, 50);
		profil.add(btnLogout);

		lblNewLabel = new JLabel(mysqlDateString);
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("Serif", Font.BOLD, 18));
		lblNewLabel.setBounds(9, 13, 178, 45);
		profil.add(lblNewLabel);

		isi = new JPanel();
		isi.setBounds(193, 0, 1179, 693);
		isi.setForeground(new Color(102, 255, 102));
		isi.setBackground(new Color(152, 251, 152));
		getContentPane().add(isi);
		isi.setLayout(null);

		lblWelcomeToMoney = new JLabel("Welcome to Money Manager");
		lblWelcomeToMoney.setHorizontalAlignment(SwingConstants.CENTER);
		lblWelcomeToMoney.setFont(new Font("Papyrus", Font.BOLD, 30));
		lblWelcomeToMoney.setBounds(391, 31, 442, 51);
		isi.add(lblWelcomeToMoney);

		lblNowYourBalance = new JLabel("Now Your Balance ");
		lblNowYourBalance.setHorizontalAlignment(SwingConstants.CENTER);
		lblNowYourBalance.setFont(new Font("Papyrus", Font.BOLD, 28));
		lblNowYourBalance.setBounds(414, 128, 373, 41);
		isi.add(lblNowYourBalance);

		panel_1 = new JPanel();
		panel_1.setBounds(325, 202, 553, 379);
		panel_1.setBackground(new Color(152, 251, 152));
		isi.add(panel_1);
		panel_1.setLayout(null);

		lblNewLabel_2 = new JLabel("Rp");
		lblNewLabel_2.setBounds(34, 73, 111, 36);
		lblNewLabel_2.setFont(new Font("Serif", Font.BOLD, 27));
		lblNewLabel_2.setHorizontalAlignment(SwingConstants.RIGHT);
		panel_1.add(lblNewLabel_2);

		lb = new JLabel("");
		lb.setBounds(240, 73, 148, 41);
		lb.setFont(new Font("Serif", Font.BOLD, 27));
		lb.setHorizontalAlignment(SwingConstants.RIGHT);
		panel_1.add(lb);

		btnSetUangAnda = new JButton("");
		btnSetUangAnda.setBorder(BorderFactory.createEmptyBorder());
		btnSetUangAnda.setBackground(new Color(152, 251, 152));
		Image tambah = new ImageIcon(this.getClass().getResource(
				"image/unnamed.png")).getImage();
		btnSetUangAnda.setIcon(new ImageIcon(tambah));
		btnSetUangAnda.setBounds(17, 238, 128, 128);
		btnSetUangAnda.addActionListener(this);
		panel_1.add(btnSetUangAnda);

		btnKurangUang = new JButton("");
		btnKurangUang.setBorder(BorderFactory.createEmptyBorder());
		btnKurangUang.setBackground(new Color(152, 251, 152));
		Image kurang = new ImageIcon(this.getClass().getResource(
				"image/unnamed (1).png")).getImage();
		btnKurangUang.setIcon(new ImageIcon(kurang));
		btnKurangUang.setBounds(413, 238, 128, 128);
		btnKurangUang.addActionListener(this);
		panel_1.add(btnKurangUang);

		paneladd = new JPanel();
		paneladd.setBounds(325, 202, 553, 379);
		paneladd.setBackground(new Color(152, 251, 152));
		// isi.add(paneladd);
		paneladd.setLayout(null);

		lbDeposit = new JLabel("Cash Rp");
		lbDeposit.setHorizontalAlignment(SwingConstants.CENTER);
		lbDeposit.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lbDeposit.setBounds(34, 73, 150, 36);
		paneladd.add(lbDeposit);

		txtcash = new JTextField();
		txtcash.setBounds(350, 73, 148, 41);
		txtcash.setColumns(10);
		paneladd.add(txtcash);

		lblCashRp = new JLabel("Keterangan");
		lblCashRp.setHorizontalAlignment(SwingConstants.CENTER);
		lblCashRp.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblCashRp.setBounds(34, 123, 150, 36);
		paneladd.add(lblCashRp);

		txtdeposit = new JTextField();
		txtdeposit.setBounds(350, 123, 148, 41);
		txtdeposit.setColumns(10);
		paneladd.add(txtdeposit);

		btncancel = new JButton("Cancel");
		btncancel.setEnabled(true);
		btncancel.setBounds(350, 250, 150, 46);
		btncancel.setOpaque(true);
		btncancel.setContentAreaFilled(true);
		btncancel.setBackground(new Color(245, 222, 179));
		btncancel.addActionListener(this);
		paneladd.add(btncancel);

		btnSave = new JButton("Save");
		btnSave.setEnabled(true);
		btnSave.setBounds(34, 250, 150, 46);
		btnSave.setOpaque(true);
		btnSave.setContentAreaFilled(true);
		btnSave.setBackground(new Color(245, 222, 179));
		btnSave.addActionListener(this);
		paneladd.add(btnSave);

		panelminus = new JPanel();
		panelminus.setBounds(325, 202, 553, 379);
		panelminus.setBackground(new Color(152, 251, 152));
		// isi.add(panelminus);
		panelminus.setLayout(null);

		lbketer = new JLabel("Keterangan");
		lbketer.setHorizontalAlignment(SwingConstants.CENTER);
		lbketer.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lbketer.setBounds(34, 123, 150, 36);
		panelminus.add(lbketer);

		txtketerangan = new JTextField();
		txtketerangan.setHorizontalAlignment(SwingConstants.CENTER);
		txtketerangan.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtketerangan.setBounds(350, 123, 150, 36);
		panelminus.add(txtketerangan);

		lbkurang = new JLabel("Jumlah Rp");
		lbkurang.setHorizontalAlignment(SwingConstants.CENTER);
		lbkurang.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lbkurang.setBounds(34, 73, 150, 36);
		panelminus.add(lbkurang);

		txtkurang = new JTextField();
		txtkurang.setBounds(350, 73, 148, 41);
		txtkurang.setColumns(10);
		panelminus.add(txtkurang);

		btnSubmit = new JButton("Submit");
		btnSubmit.setEnabled(true);
		btnSubmit.setBounds(34, 250, 150, 46);
		btnSubmit.setOpaque(true);
		btnSubmit.setContentAreaFilled(true);
		btnSubmit.setBackground(new Color(245, 222, 179));
		btnSubmit.addActionListener(this);
		panelminus.add(btnSubmit);

		btncancel1 = new JButton("Cancel");
		btncancel1.setEnabled(true);
		btncancel1.setBounds(350, 250, 150, 46);
		btncancel1.setOpaque(true);
		btncancel1.setContentAreaFilled(true);
		btncancel1.setBackground(new Color(245, 222, 179));
		btncancel1.addActionListener(this);
		panelminus.add(btncancel1);

		pnlreport = new JPanel();
		pnlreport.setBounds(193, 0, 1179, 693);
		pnlreport.setForeground(new Color(102, 255, 102));
		pnlreport.setBackground(new Color(152, 251, 152));

		pnlreport.setLayout(null);

		lblreport = new JLabel("Your Balance Report");
		lblreport.setHorizontalAlignment(SwingConstants.CENTER);
		lblreport.setFont(new Font("Papyrus", Font.BOLD, 30));
		lblreport.setBounds(414, 50, 379, 44);
		pnlreport.add(lblreport);

		Image rept = new ImageIcon(this.getClass()
				.getResource("image/rept.png")).getImage();
		Image saving = new ImageIcon(this.getClass().getResource(
				"image/saving.png")).getImage();
		Image grafik = new ImageIcon(this.getClass().getResource(
				"image/grafik.png")).getImage();
		Image pdf = new ImageIcon(this.getClass()
				.getResource("image/print.png")).getImage();
		Image cash = new ImageIcon(this.getClass()
				.getResource("image/cash.png")).getImage();

		btnrept = new JButton();
		btnrept.setBounds(200, 150, 72, 72);
		btnrept.setBorder(BorderFactory.createEmptyBorder());
		btnrept.setBackground(new Color(152, 251, 152));
		btnrept.setIcon(new ImageIcon(rept));
		btnrept.addActionListener(this);
		btnrept.setToolTipText("Report Pengeluaran");
		pnlreport.add(btnrept);

		btnsaving = new JButton();
		btnsaving.setBounds(400, 150, 72, 72);
		btnsaving.setBorder(BorderFactory.createEmptyBorder());
		btnsaving.setBackground(new Color(152, 251, 152));
		btnsaving.setIcon(new ImageIcon(saving));
		btnsaving.addActionListener(this);
		btnsaving.setToolTipText("Report Bank");
		pnlreport.add(btnsaving);

		btncash = new JButton();
		btncash.setBounds(600, 150, 72, 72);
		btncash.setBorder(BorderFactory.createEmptyBorder());
		btncash.setBackground(new Color(152, 251, 152));
		btncash.setIcon(new ImageIcon(cash));
		btncash.addActionListener(this);
		btncash.setToolTipText("Report Cash");
		pnlreport.add(btncash);

		btngrafik = new JButton();
		btngrafik.setBounds(800, 150, 72, 72);
		btngrafik.setBorder(BorderFactory.createEmptyBorder());
		btngrafik.setBackground(new Color(152, 251, 152));
		btngrafik.setIcon(new ImageIcon(grafik));
		btngrafik.addActionListener(this);
		btngrafik.setToolTipText("Grafik");
		pnlreport.add(btngrafik);

		btnpdf = new JButton("Print");
		btnpdf.setFont(new Font("Arial", Font.BOLD, 15));
		btnpdf.setBounds(960, 650, 120, 40);
		// btnpdf.setBorder(BorderFactory.createEmptyBorder());
		btnpdf.setBackground(new Color(152, 251, 152));
		btnpdf.setIcon(new ImageIcon(pdf));
		btnpdf.addActionListener(this);
		btnpdf.setToolTipText("Print Report");

		btnpdf1 = new JButton("Print");
		btnpdf1.setFont(new Font("Arial", Font.BOLD, 15));
		btnpdf1.setBounds(960, 650, 120, 40);
		// btnpdf.setBorder(BorderFactory.createEmptyBorder());
		btnpdf1.setBackground(new Color(152, 251, 152));
		btnpdf1.setIcon(new ImageIcon(pdf));
		btnpdf1.addActionListener(this);
		btnpdf1.setToolTipText("Print Report");

		btnpdf2 = new JButton("Print");
		btnpdf2.setFont(new Font("Arial", Font.BOLD, 15));
		btnpdf2.setBounds(960, 650, 120, 40);
		// btnpdf.setBorder(BorderFactory.createEmptyBorder());
		btnpdf2.setBackground(new Color(152, 251, 152));
		btnpdf2.setIcon(new ImageIcon(pdf));
		btnpdf2.addActionListener(this);
		btnpdf2.setToolTipText("Print Report");

		tabReport = new JTable();
		String[] colName = { "Tanggal", "Kategori", "Pengeluaran", "Uang Cash",
				"Sisa" };
		tabReport.setPreferredScrollableViewportSize(new Dimension(450, 200));
		tabReport.setFillsViewportHeight(true);
		dbModel = new DefaultTableModel(null, colName);
		tabReport.setModel(dbModel);
		tblscroll = new JScrollPane(tabReport);
		tblscroll.setSize(955, 394);
		tblscroll.setLocation(100, 250);

		lbmasuk = new JLabel("Pemasukkan");
		lbmasuk.setFont(new Font("Arial", Font.BOLD, 15));
		lbmasuk.setBounds(100, 200, 200, 80);

		tabReport1 = new JTable();
		String[] colName1 = { "Tanggal", "Tambahan Cash", "Keterangan",
				"Total Cash" };
		dbModel1 = new DefaultTableModel(null, colName1);
		tabReport1.setPreferredScrollableViewportSize(new Dimension(450, 200));
		tabReport1.setFillsViewportHeight(true);
		tabReport1.setModel(dbModel1);
		tblscroll1 = new JScrollPane(tabReport1);
		tblscroll1.setSize(450, 394);
		tblscroll1.setLocation(100, 250);

		lbpinjam = new JLabel("Pengurangan di luar kategori");
		lbpinjam.setBounds(650, 200, 220, 80);
		lbpinjam.setFont(new Font("Arial", Font.BOLD, 15));

		tabReport2 = new JTable();
		String[] colName2 = { "Tanggal", "Pengurangan Cash", "Keterangan",
				"Total Cash" };
		dbModel2 = new DefaultTableModel(null, colName2);
		tabReport2.setPreferredScrollableViewportSize(new Dimension(450, 200));
		tabReport2.setFillsViewportHeight(true);
		tabReport2.setModel(dbModel2);
		tblscroll2 = new JScrollPane(tabReport2);
		tblscroll2.setSize(450, 394);
		tblscroll2.setLocation(650, 250);

		lbsaving = new JLabel("Deposito Bank");
		lbsaving.setFont(new Font("Arial", Font.BOLD, 15));
		lbsaving.setBounds(100, 200, 200, 80);

		tabReport3 = new JTable();
		String[] colName3 = { "Tanggal", "Tambahan", "Total Deposit" };
		dbModel3 = new DefaultTableModel(null, colName3);
		tabReport3.setPreferredScrollableViewportSize(new Dimension(450, 200));
		tabReport3.setFillsViewportHeight(true);
		tabReport3.setModel(dbModel3);
		tblscroll3 = new JScrollPane(tabReport3);
		tblscroll3.setSize(450, 394);
		tblscroll3.setLocation(100, 250);

		lbtarik = new JLabel("Penenarikkan ATM");
		lbtarik.setBounds(650, 200, 220, 80);
		lbtarik.setFont(new Font("Arial", Font.BOLD, 15));

		tabReport4 = new JTable();
		String[] colName4 = { "Tanggal", "Penarikkan", "Total Deposit" };
		dbModel4 = new DefaultTableModel(null, colName4);
		tabReport4.setPreferredScrollableViewportSize(new Dimension(450, 200));
		tabReport4.setFillsViewportHeight(true);
		tabReport4.setModel(dbModel4);
		tblscroll4 = new JScrollPane(tabReport4);
		tblscroll4.setSize(450, 394);
		tblscroll4.setLocation(650, 250);

		tanggal = new JComboBox();
		tanggal.setBounds(150, 250, 150, 35);
		tanggal.setFont(new Font("Arial", Font.BOLD, 14));
		tanggal.addActionListener(this);

		pnlkategori = new JPanel();
		pnlkategori.setBounds(190, 0, 1175, 693);
		pnlkategori.setForeground(new Color(102, 255, 102));
		pnlkategori.setBackground(new Color(152, 251, 152));
		pnlkategori.setLayout(null);

		lblkate = new JLabel("Category List");
		lblkate.setHorizontalAlignment(SwingConstants.CENTER);
		lblkate.setFont(new Font("Papyrus", Font.BOLD, 30));
		lblkate.setBounds(414, 108, 379, 44);
		pnlkategori.add(lblkate);

		btnbook = new JButton();
		btnbook.setBorder(BorderFactory.createEmptyBorder());
		btnbook.setBackground(new Color(152, 251, 152));
		Image book = new ImageIcon(this.getClass().getResource(
				"image/buku72.png")).getImage();
		btnbook.setIcon(new ImageIcon(book));
		btnbook.setBounds(200, 250, 72, 72);
		btnbook.addActionListener(this);
		lbbook = new JLabel("Bacaan");
		lbbook.setFont(new Font("Arial", Font.BOLD, 14));
		lbbook.setHorizontalAlignment(SwingConstants.CENTER);
		lbbook.setBounds(200, 330, 72, 30);
		pnlkategori.add(btnbook);
		pnlkategori.add(lbbook);

		btnlistrik = new JButton();
		btnlistrik.setBorder(BorderFactory.createEmptyBorder());
		btnlistrik.setBackground(new Color(152, 251, 152));
		Image listrik = new ImageIcon(this.getClass().getResource(
				"image/listrik72.png")).getImage();
		btnlistrik.setIcon(new ImageIcon(listrik));
		btnlistrik.setBounds(200, 400, 72, 72);
		btnlistrik.addActionListener(this);
		lblistrik = new JLabel("Tarif Listrik");
		lblistrik.setFont(new Font("Arial", Font.BOLD, 14));
		lblistrik.setHorizontalAlignment(SwingConstants.CENTER);
		lblistrik.setBounds(200, 490, 82, 30);
		pnlkategori.add(btnlistrik);
		pnlkategori.add(lblistrik);

		btntransport = new JButton();
		btntransport.setBorder(BorderFactory.createEmptyBorder());
		btntransport.setBackground(new Color(152, 251, 152));
		Image transport = new ImageIcon(this.getClass().getResource(
				"image/car72.png")).getImage();
		btntransport.setIcon(new ImageIcon(transport));
		btntransport.setBounds(200, 550, 72, 72);
		btntransport.addActionListener(this);
		lbtransport = new JLabel("Tarif Transport");
		lbtransport.setFont(new Font("Arial", Font.BOLD, 14));
		lbtransport.setHorizontalAlignment(SwingConstants.CENTER);
		lbtransport.setBounds(200, 630, 104, 30);
		pnlkategori.add(btntransport);
		pnlkategori.add(lbtransport);

		btnmakan = new JButton();
		btnmakan.setBorder(BorderFactory.createEmptyBorder());
		btnmakan.setBackground(new Color(152, 251, 152));
		Image makan = new ImageIcon(this.getClass().getResource(
				"image/food72.png")).getImage();
		btnmakan.setIcon(new ImageIcon(makan));
		btnmakan.setBounds(400, 250, 72, 72);
		btnmakan.addActionListener(this);
		lbmakan = new JLabel("Makan");
		lbmakan.setFont(new Font("Arial", Font.BOLD, 14));
		lbmakan.setHorizontalAlignment(SwingConstants.CENTER);
		lbmakan.setBounds(400, 330, 72, 30);
		pnlkategori.add(btnmakan);
		pnlkategori.add(lbmakan);

		btnbelanja = new JButton();
		btnbelanja.setBorder(BorderFactory.createEmptyBorder());
		btnbelanja.setBackground(new Color(152, 251, 152));
		Image belanja = new ImageIcon(this.getClass().getResource(
				"image/shopping24.png")).getImage();
		btnbelanja.setIcon(new ImageIcon(belanja));
		btnbelanja.setBounds(400, 400, 72, 72);
		btnbelanja.addActionListener(this);
		lbbelanja = new JLabel("Belanja");
		lbbelanja.setFont(new Font("Arial", Font.BOLD, 14));
		lbbelanja.setHorizontalAlignment(SwingConstants.CENTER);
		lbbelanja.setBounds(400, 490, 72, 30);
		pnlkategori.add(btnbelanja);
		pnlkategori.add(lbbelanja);

		btnpulsa = new JButton();
		btnpulsa.setBorder(BorderFactory.createEmptyBorder());
		btnpulsa.setBackground(new Color(152, 251, 152));
		Image pulsa = new ImageIcon(this.getClass().getResource(
				"image/phone72.png")).getImage();
		btnpulsa.setIcon(new ImageIcon(pulsa));
		btnpulsa.setBounds(400, 550, 72, 72);
		btnpulsa.addActionListener(this);
		lbpulsa = new JLabel("Pulsa");
		lbpulsa.setFont(new Font("Arial", Font.BOLD, 14));
		lbpulsa.setHorizontalAlignment(SwingConstants.CENTER);
		lbpulsa.setBounds(400, 630, 72, 30);
		pnlkategori.add(btnpulsa);
		pnlkategori.add(lbpulsa);

		btnair = new JButton();
		btnair.setBorder(BorderFactory.createEmptyBorder());
		btnair.setBackground(new Color(152, 251, 152));
		Image air = new ImageIcon(this.getClass().getResource(
				"image/pipaair.png")).getImage();
		btnair.setIcon(new ImageIcon(air));
		btnair.setBounds(600, 250, 72, 72);
		btnair.addActionListener(this);
		lbair = new JLabel("Tarif Air");
		lbair.setFont(new Font("Arial", Font.BOLD, 14));
		lbair.setHorizontalAlignment(SwingConstants.CENTER);
		lbair.setBounds(600, 330, 82, 30);
		pnlkategori.add(btnair);
		pnlkategori.add(lbair);

		btninternet = new JButton();
		btninternet.setBorder(BorderFactory.createEmptyBorder());
		btninternet.setBackground(new Color(152, 251, 152));
		Image internet = new ImageIcon(this.getClass().getResource(
				"image/internet72.png")).getImage();
		btninternet.setIcon(new ImageIcon(internet));
		btninternet.setBounds(600, 400, 72, 72);
		btninternet.addActionListener(this);
		lbinternet = new JLabel("Tarif Internet");
		lbinternet.setFont(new Font("Arial", Font.BOLD, 14));
		lbinternet.setHorizontalAlignment(SwingConstants.CENTER);
		lbinternet.setBounds(600, 490, 97, 30);
		pnlkategori.add(btninternet);
		pnlkategori.add(lbinternet);

		btnjajan = new JButton();
		btnjajan.setBorder(BorderFactory.createEmptyBorder());
		btnjajan.setBackground(new Color(152, 251, 152));
		Image jajan = new ImageIcon(this.getClass().getResource(
				"image/dompet72.png")).getImage();
		btnjajan.setIcon(new ImageIcon(jajan));
		btnjajan.setBounds(800, 250, 72, 72);
		btnjajan.addActionListener(this);
		lbjajan = new JLabel("Jajan");
		lbjajan.setFont(new Font("Arial", Font.BOLD, 14));
		lbjajan.setHorizontalAlignment(SwingConstants.CENTER);
		lbjajan.setBounds(800, 330, 82, 30);
		pnlkategori.add(btnjajan);
		pnlkategori.add(lbjajan);

		btnhiburan = new JButton();
		btnhiburan.setBorder(BorderFactory.createEmptyBorder());
		btnhiburan.setBackground(new Color(152, 251, 152));
		Image hiburan = new ImageIcon(this.getClass().getResource(
				"image/entertain72.png")).getImage();
		btnhiburan.setIcon(new ImageIcon(hiburan));
		btnhiburan.setBounds(800, 400, 72, 72);
		btnhiburan.addActionListener(this);
		lbhiburan = new JLabel("Hiburan");
		lbhiburan.setFont(new Font("Arial", Font.BOLD, 14));
		lbhiburan.setHorizontalAlignment(SwingConstants.CENTER);
		lbhiburan.setBounds(800, 490, 82, 30);
		pnlkategori.add(btnhiburan);
		pnlkategori.add(lbhiburan);

		btnbaju = new JButton();
		btnbaju.setBorder(BorderFactory.createEmptyBorder());
		btnbaju.setBackground(new Color(152, 251, 152));
		Image baju = new ImageIcon(this.getClass().getResource(
				"image/cloth72.png")).getImage();
		btnbaju.setIcon(new ImageIcon(baju));
		btnbaju.setBounds(600, 550, 72, 72);
		btnbaju.addActionListener(this);
		lbbaju = new JLabel("Pakaian");
		lbbaju.setFont(new Font("Arial", Font.BOLD, 14));
		lbbaju.setHorizontalAlignment(SwingConstants.CENTER);
		lbbaju.setBounds(600, 630, 82, 30);
		pnlkategori.add(btnbaju);
		pnlkategori.add(lbbaju);

		btntabungan = new JButton();
		btntabungan.setBorder(BorderFactory.createEmptyBorder());
		btntabungan.setBackground(new Color(152, 251, 152));
		Image bank = new ImageIcon(this.getClass().getResource(
				"image/bank72.png")).getImage();
		btntabungan.setIcon(new ImageIcon(bank));
		btntabungan.setBounds(800, 550, 72, 72);
		btntabungan.addActionListener(this);
		lbtabungan = new JLabel("Tabungan");
		lbtabungan.setFont(new Font("Arial", Font.BOLD, 14));
		lbtabungan.setHorizontalAlignment(SwingConstants.CENTER);
		lbtabungan.setBounds(800, 630, 82, 30);
		pnlkategori.add(btntabungan);
		pnlkategori.add(lbtabungan);

		ShowReport(a);
		b = lblNewLabel.getText();
	}

	private static Connection getConnection() throws Exception {
		Class.forName("com.mysql.jdbc.Driver");
		String url = "jdbc:mysql://localhost/monefy";
		String user = "root";
		String pass = "";
		return DriverManager.getConnection(url, user, pass);
	}

	public void datacash(String a) {
		try {
			con = getConnection();
			PreparedStatement stm = con
					.prepareStatement("select distinct * from reportaddcash where username='"
							+ a + "'");
			ResultSet rst = stm.executeQuery();

			while (rst.next()) {
				Date dat1 = rst.getDate(3);
				DateFormat df1 = new SimpleDateFormat("dd-MM-yyyy");
				String h1 = df1.format(dat1);
				dbModel1.addRow(new Object[] { h1, rst.getString(5),
						rst.getString(6), rst.getString(7) });
			}
			stm.close();
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "koneki anda");
		}
	}

	public void bankdata(String a) {
		try {
			con = getConnection();
			PreparedStatement stm2 = con
					.prepareStatement("select distinct * from reportaddbank where username ='"
							+ a + "'");
			ResultSet rs4 = stm2.executeQuery();

			while (rs4.next()) {
				Date dat2 = rs4.getDate(3);
				DateFormat df2 = new SimpleDateFormat("dd-MM-yyyy");
				String h2 = df2.format(dat2);
				dbModel3.addRow(new Object[] { h2, rs4.getString(5),
						rs4.getString(6) });
			}
		} catch (Exception e) {

			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "koneki anda");
		}
	}

	public void atmdata(String a) {
		try {
			con = getConnection();
			PreparedStatement stm2 = con
					.prepareStatement("SELECT DISTINCT * FROM penarikanbank WHERE username = '"
							+ a + "'");
			ResultSet rs1 = stm2.executeQuery();

			while (rs1.next()) {
				Date dat3 = rs1.getDate(3);
				DateFormat df3 = new SimpleDateFormat("dd-MM-yyyy");
				String h3 = df3.format(dat3);
				dbModel4.addRow(new Object[] { h3, rs1.getString(5),
						rs1.getString(6) });
			}
		} catch (Exception e) {

			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "koneki anda");
		}
	}

	public void datakurang(String a) {
		try {
			con = getConnection();
			PreparedStatement stm1 = con
					.prepareStatement("Select Distinct * from pengurangancash where username='"
							+ a + "'");
			ResultSet stm = stm1.executeQuery();
			while (stm.next()) {
				Date dat = stm.getDate(3);
				DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
				String h = df.format(dat);
				dbModel2.addRow(new Object[] { h, stm.getString(5),
						stm.getString(6), stm.getString(7) });
			}
			stm1.close();
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "Cek Koneki anda");
		}
	}

	public void dataTable(String a) {
		try {
			con = getConnection();
			PreparedStatement st = con
					.prepareStatement("select  Distinct * from pengeluaran where username='"
							+ a + "'");
			ResultSet rs = st.executeQuery();
			while (rs.next()) {
				Date dat = rs.getDate(3);
				DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
				String h = df.format(dat);
				dbModel.addRow(new Object[] { h, rs.getString(4),
						rs.getString(5), rs.getString(6), rs.getString(7) });
			}

			st.close();
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "koneki anda");
		}
	}

	public void cleardata1(String a) {

		while (dbModel1.getRowCount() > 0) {
			for (int i = 0; i < dbModel1.getRowCount(); i++) {
				dbModel1.removeRow(i);
			}
		}
	}

	public void cleardata2(String a) {

		while (dbModel2.getRowCount() > 0) {
			for (int i = 0; i < dbModel2.getRowCount(); i++) {
				dbModel2.removeRow(i);
			}
		}
	}

	public void cleardata3(String a) {

		while (dbModel3.getRowCount() > 0) {
			for (int i = 0; i < dbModel3.getRowCount(); i++) {
				dbModel3.removeRow(i);
			}
		}
	}

	public void cleardata4(String a) {

		while (dbModel4.getRowCount() > 0) {
			for (int i = 0; i < dbModel4.getRowCount(); i++) {
				dbModel4.removeRow(i);
			}
		}
	}

	public void cleardata(String a) {

		while (dbModel.getRowCount() > 0) {
			for (int i = 0; i < dbModel.getRowCount(); i++) {
				dbModel.removeRow(i);
			}
		}
	}

	public void ShowReport(String a) {
		try {
			con = getConnection();
			String sql = "SELECT cash.ugmasuk from cash where username='" + a
					+ "'";
			Statement set = con.createStatement();
			ResultSet rs = set.executeQuery(sql);
			while (rs.next()) {
				lb.setText(rs.getString("cash.ugmasuk"));
			}
			set.close();
			con.close();
			setDefaultCloseOperation(EXIT_ON_CLOSE);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "terjadi kesalahan");
		}
	}

	public void lapor(String a) {
		try {
			con = getConnection();
			PreparedStatement st = con
					.prepareStatement("INSERT INTO pengurangancash values(default,?,?,?,?,?,?) ");
			PreparedStatement st2 = con
					.prepareStatement("UPDATE cash set ugmasuk=? where username='"
							+ a + "'");
			st.setString(1, lbuser.getText());
			st.setString(2, lblNewLabel.getText());
			st.setString(3, lb.getText());
			st.setString(4, txtkurang.getText());
			st.setString(5, txtketerangan.getText());
			st.setString(6, total1.getText());
			st2.setString(1, total1.getText());
			st.executeUpdate();
			st2.executeUpdate();
			st.close();
			st2.close();
			con.close();
		} catch (Exception e1) {
			JOptionPane.showMessageDialog(null, "Cek Koneksi Anda");
		}
	}

	public void Addcash() {
		int cash, cash1, tot;
		String awal = lb.getText();
		cash = Integer.parseInt(awal);
		String add = txtcash.getText();
		cash1 = Integer.parseInt(add);
		tot = cash + cash1;
		total = new JLabel(Integer.toString(tot));
	}

	public void Minuscash() {
		int awal, tot1, min;
		String awal1 = lb.getText();
		awal = Integer.parseInt(awal1);
		String kurng = txtkurang.getText();
		min = Integer.parseInt(kurng);
		if (awal >= min) {
			tot1 = awal - min;
			total1 = new JLabel(Integer.toString(tot1));
			lapor(a);
		} else {
			JOptionPane.showMessageDialog(null, "Saldo Anda tidak mencukupi");
		}
	}

	public void graph(String a) {
		try {
			con = getConnection();
			PreparedStatement st1 = con
					.prepareStatement("Select distinct tanggal from pengeluaran where username='"
							+ a + "'");
			ResultSet ss = st1.executeQuery();
			while (ss.next()) {
				String add = ss.getString("tanggal");
				tanggal.addItem(add);
			}

		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Cek Koneksi Anda");
		}
	}

	public void grafikfix() {
		try {
			String tgl = tanggal.getSelectedItem().toString();
			con = getConnection();
			PreparedStatement te = con
					.prepareStatement("SELECT DISTINCT DATE('"
							+ tgl
							+ "'), kategori, SUM( value ) AS total FROM pengeluaran WHERE username = '"
							+ a + "' AND tanggal = DATE( '" + tgl
							+ "') GROUP BY kategori");
			ResultSet sd = te.executeQuery();
			DefaultPieDataset dataset = new DefaultPieDataset();
			while (sd.next()) {
				dataset.setValue(sd.getString("kategori"),
						Double.parseDouble(sd.getString("total")));
			}
			JFreeChart chart = ChartFactory.createPieChart(
					"Garfik Pengeluaran", dataset, true, true, false);
			ChartPanel e = new ChartPanel(chart);
			pnlreport.add(e);
			e.setDomainZoomable(true);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Cek Koneksi Anda");
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if (e.getSource() == btnEntryData) {
			remove(isi);
			remove(pnlreport);
			remove(pnlkategori);
			getContentPane().add(pnlkategori);
			repaint();
		} else if (e.getSource() == btnSetUangAnda) {
			isi.add(panel_1).setVisible(false);
			isi.add(panelminus).setVisible(false);
			isi.add(paneladd).setVisible(true);
			repaint();
		} else if (e.getSource() == btnSave) {

			if (txtdeposit.getText().equals("") || txtcash.getText().equals("")) {
				JOptionPane.showMessageDialog(null, "Isi Fieldnya!");

			} else {
				try {
					Addcash();
					con = getConnection();
					PreparedStatement st = con
							.prepareStatement("UPDATE cash set ugmasuk=? where username='"
									+ a + "'");
					PreparedStatement st2 = con
							.prepareStatement("INSERT INTO reportaddcash values (default,?,?,?,?,?,?)");

					st.setString(1, total.getText());

					st2.setString(1, lbuser.getText());
					st2.setString(2, lblNewLabel.getText());
					st2.setString(3, lb.getText());
					st2.setString(4, txtcash.getText());
					st2.setString(5, txtdeposit.getText());
					st2.setString(6, total.getText());

					st.executeUpdate();
					st2.executeUpdate();
					JOptionPane.showMessageDialog(null, "Terima Kasih");
					st.close();
					st2.close();
					con.close();
					txtcash.setText("");
					txtdeposit.setText("");
					setDefaultCloseOperation(EXIT_ON_CLOSE);
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(null, "Cek koneksi anda");
				}
				ShowReport(a);
				isi.add(panel_1).setVisible(true);
				isi.add(paneladd).setVisible(false);
				isi.add(panelminus).setVisible(false);
				repaint();
			}
		} else if (e.getSource() == btnReport) {
			remove(isi);
			remove(pnlreport);
			remove(pnlkategori);
			ShowReport(a);
			getContentPane().add(isi);
			repaint();
		} else if (e.getSource() == btncancel) {
			ShowReport(a);
			isi.add(panel_1).setVisible(true);
			isi.add(paneladd).setVisible(false);
			isi.add(panelminus).setVisible(false);
			repaint();
		} else if (e.getSource() == btncancel1) {
			ShowReport(a);
			isi.add(panel_1).setVisible(true);
			isi.add(paneladd).setVisible(false);
			isi.add(panelminus).setVisible(false);
			repaint();
		} else if (e.getSource() == btnKurangUang) {
			isi.add(panel_1).setVisible(false);
			isi.add(paneladd).setVisible(false);
			isi.add(panelminus).setVisible(true);
			repaint();
		} else if (e.getSource() == btnSubmit) {
			if (txtkurang.getText().equals("") || txtketerangan.equals("")) {
				JOptionPane.showMessageDialog(null, "Isi Fieldnya");
			} else {
				Minuscash();
				txtkurang.setText("");
				txtketerangan.setText("");
				ShowReport(a);
				isi.add(panel_1).setVisible(true);
				isi.add(paneladd).setVisible(false);
				isi.add(panelminus).setVisible(false);
				repaint();
			}
		} else if (e.getSource() == btnDetail) {
			remove(isi);
			remove(pnlkategori);
			getContentPane().add(pnlreport);
			pnlreport.remove(lbmasuk);
			pnlreport.remove(lbpinjam);
			pnlreport.remove(lbsaving);
			pnlreport.remove(lbtarik);
			pnlreport.remove(tblscroll2);
			pnlreport.remove(tblscroll1);
			pnlreport.remove(tblscroll3);
			pnlreport.remove(tblscroll4);
			pnlreport.remove(btnpdf1);
			pnlreport.remove(btnpdf2);
			pnlreport.add(tblscroll);

			pnlreport.add(btnpdf);

			cleardata(a);
			dataTable(a);
			repaint();
		} else if (e.getSource() == btnbook) {
			remove(isi);
			remove(pnlreport);
			book buku = new book(a, b);
			buku.setVisible(true);
			repaint();
		} else if (e.getSource() == btnlistrik) {
			remove(isi);
			remove(pnlreport);
			listrik listrik1 = new listrik(a, b);
			listrik1.setVisible(true);
			repaint();
		} else if (e.getSource() == btntransport) {
			remove(isi);
			remove(pnlreport);
			transport car = new transport(a, b);
			car.setVisible(true);
			repaint();
		} else if (e.getSource() == btnmakan) {
			remove(isi);
			remove(pnlreport);
			makan makan = new makan(a, b);
			makan.setVisible(true);
			repaint();
		} else if (e.getSource() == btnbelanja) {
			remove(isi);
			remove(pnlreport);
			belanja belanja = new belanja(a, b);
			belanja.setVisible(true);
			repaint();
		} else if (e.getSource() == btnpulsa) {
			remove(isi);
			remove(pnlreport);
			pulsa pulsa = new pulsa(a, b);
			pulsa.setVisible(true);
			repaint();
		} else if (e.getSource() == btnair) {
			remove(isi);
			remove(pnlreport);
			air air = new air(a, b);
			air.setVisible(true);
			repaint();
		} else if (e.getSource() == btninternet) {
			remove(isi);
			remove(pnlreport);
			internet internet = new internet(a, b);
			internet.setVisible(true);
			repaint();
		} else if (e.getSource() == btnjajan) {
			remove(isi);
			remove(pnlreport);
			jajan jajan = new jajan(a, b);
			jajan.setVisible(true);
			repaint();
		} else if (e.getSource() == btnhiburan) {
			remove(isi);
			remove(pnlreport);
			hiburan hiburan = new hiburan(a, b);
			hiburan.setVisible(true);
			repaint();
		} else if (e.getSource() == btnbaju) {
			remove(isi);
			remove(pnlreport);
			baju dress = new baju(a, b);
			dress.setVisible(true);
			repaint();
		} else if (e.getSource() == btntabungan) {
			remove(isi);
			remove(pnlreport);
			depositadd add = new depositadd(a, b);
			add.setVisible(true);
			repaint();
		} else if (e.getSource() == btnrept) {
			remove(isi);
			remove(pnlkategori);
			pnlreport.remove(lbmasuk);
			pnlreport.remove(lbpinjam);
			pnlreport.remove(lbsaving);
			pnlreport.remove(lbtarik);
			pnlreport.remove(tblscroll2);
			pnlreport.remove(tblscroll1);
			pnlreport.remove(tblscroll3);
			pnlreport.remove(tblscroll4);
			pnlreport.remove(btnpdf1);
			pnlreport.remove(btnpdf2);
			pnlreport.add(tblscroll);
			pnlreport.add(btnpdf);
			cleardata(a);
			dataTable(a);
			repaint();
		} else if (e.getSource() == btncash) {
			remove(isi);
			remove(pnlkategori);
			pnlreport.remove(tblscroll);
			pnlreport.remove(tblscroll3);
			pnlreport.remove(tblscroll4);
			pnlreport.remove(lbsaving);
			pnlreport.remove(lbtarik);
			pnlreport.remove(btnpdf);
			pnlreport.remove(btnpdf2);
			pnlreport.add(lbmasuk);
			pnlreport.add(lbpinjam);
			pnlreport.add(tblscroll1);
			pnlreport.add(tblscroll2);
			pnlreport.add(btnpdf1);
			cleardata1(a);
			datacash(a);
			cleardata2(a);
			datakurang(a);
			repaint();
		} else if (e.getSource() == btngrafik) {
			remove(isi);
			remove(pnlkategori);
			grafik chart = new grafik(a);
			chart.setVisible(true);
			// graph(a);
			// grafikfix();
			repaint();
		} else if (e.getSource() == btnsaving) {
			remove(isi);
			remove(pnlkategori);
			pnlreport.remove(tblscroll);
			pnlreport.remove(lbmasuk);
			pnlreport.remove(lbpinjam);
			pnlreport.remove(tblscroll1);
			pnlreport.remove(tblscroll2);
			pnlreport.remove(btnpdf);
			pnlreport.remove(btnpdf1);
			pnlreport.add(tblscroll3);
			pnlreport.add(tblscroll4);
			pnlreport.add(lbsaving);
			pnlreport.add(lbtarik);
			pnlreport.add(btnpdf2);
			cleardata3(a);
			cleardata4(a);
			atmdata(a);
			bankdata(a);
			repaint();
		} else if (e.getSource() == btnpdf) {
			try {
				tabReport.print();
			} catch (PrinterException pe) {
				System.err.println("Error printing: " + pe.getMessage());
			}
			repaint();
		} else if (e.getSource() == btnpdf1) {
			try {
				tabReport1.print();
				tabReport2.print();
			} catch (PrinterException pe) {
				System.err.println("Error printing: " + pe.getMessage());
			}
			repaint();
		} else if (e.getSource() == btnpdf2) {
			try {
				tabReport3.print();
				tabReport4.print();
			} catch (PrinterException pe) {
				System.err.println("Error printing: " + pe.getMessage());
			}
			repaint();
		} else if (e.getSource() == btnLogout) {
			dispose();
		}

	}
}
